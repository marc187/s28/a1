// No. 3 and 4
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(data => data.json())
.then(data => console.log(data.map(el => el.title)))

// No. 5 and 6
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then(data => data.json())
.then(data => {
    let {title, completed} = data
    console.log(title, completed)
})

// No. 7
fetch(`https://jsonplaceholder.typicode.com/todos`,{
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        userId: 1,
        title: "New To Do",
        completed: false
    })
})
.then(data => data.json())
.then(data => console.log(data))


// No. 8 and 9
fetch(`https://jsonplaceholder.typicode.com/todos/2`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated To Do",
        description: "This list is updated",
        completed: true,
        completedOn: "2022-03-07",
        userId: 2
    })
})
.then(data => data.json())
.then(data => console.log(data))

// No. 10 and 11
fetch(`https://jsonplaceholder.typicode.com/todos/5`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: true,
        updatedOn: "2022-03-07-14:30",
    })
})
.then(data => data.json())
.then(data => console.log(data))


// No. 12
fetch(`https://jsonplaceholder.typicode.com/todos/10`, {
    method: "DELETE",
})









